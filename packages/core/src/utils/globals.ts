import {ConfigDataType} from './config';
import {Response} from 'express-serve-static-core';

export const serverUpLog = (configData: ConfigDataType) => {
  const {NAME, API, PORT} = configData;
  console.log(`> ${currentDate()}: ${NAME}[${API}] UP on port ${PORT}\n`)
};

export const resErr = (res: Response, status: number, message: string, error?) => {
  console.log(`> ${currentDate()}: ERROR: ${message}`, error && error.message || '');
  return res.status(status).json({message, detail: error?.message});
};

const currentDate = () => new Date().toLocaleString('ru-RU');
