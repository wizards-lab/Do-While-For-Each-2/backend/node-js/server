import * as mongoose from 'mongoose';
import {serverUpLog} from '@dwfe/core';
import {app} from './app';
import {config} from './config';

const {PORT, DB_CONNECT_URI} = config;

mongoose.connect(DB_CONNECT_URI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true
})
  .then(() => app.listen(PORT, () => serverUpLog(config)))
  .catch(error => {
    console.log('Server Error', error.message);
    process.exit(1);
  });
