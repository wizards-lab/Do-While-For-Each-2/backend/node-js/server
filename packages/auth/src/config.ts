import {API_DEV, API_PROD, API_TEST, AppName, conf, ConfigSkeleton, Env} from '@dwfe/core';

const NAME = AppName.AUTH;
const API = '/auth';
const PORT = 7777;
const dbConnectUri = (env: Env, username, password, host, basename = NAME.toLocaleLowerCase()) =>
  `mongodb://${username}:${password}@${host}:27017/${env}_${basename}`
;

const envData: ConfigSkeleton = {
  development: {
    NAME,
    API: `${API_DEV}${API}`,
    PORT,
    DB_CONNECT_URI: dbConnectUri(Env.development, 'user', '1', 'localhost'),
  },
  test: {
    NAME,
    API: `${API_TEST}${API}`,
    PORT,
    DB_CONNECT_URI: dbConnectUri(Env.test, 'user', '1', 'localhost'),
  },
  production: {
    NAME,
    API: `${API_PROD}${API}`,
    PORT,
    DB_CONNECT_URI: dbConnectUri(Env.production, 'username', 'password', 'hostname'),
  },
};

export const config = conf(envData);
