import * as bcrypt from 'bcryptjs';
import {resErr} from '@dwfe/core';
import User from '../models/user';

export const register = async (req, res) => {
  const {email, password} = req.body;

  if (!email || !password)
    return resErr(res, 400, 'Email or password is empty error');

  return User
    .findOne({email})
    .then(candidate => candidate
      ? resErr(res, 400, 'Email present in database')
      : bcrypt.hash(password + '', 8)
        .then(hashedPassword =>
          new User({email, password: hashedPassword})
            .save()
            .then(() => res.status(201).json({message: 'User has been created'}))
            .catch(error => resErr(res, 400, 'Save user to DB error', error))
        )
        .catch(error => resErr(res, 400, 'Password hash error', error))
    )
    .catch(error => resErr(res, 400, 'Check exist user error', error));
};

export const login = (req, res) => {
  res.status(200).json({is: 'login'});
};
