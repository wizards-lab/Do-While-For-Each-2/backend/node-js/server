import authRoutes from './routes/auth';
import {ENV, Env} from '@dwfe/core';
import {config} from './config';

const {API} = config;

export const app = require('express')();

if (ENV === Env.development || ENV === Env.test) {
  app.use(require('morgan')('dev'));
  app.use(require('cors')());
  app.use((err, req, res, next) => {
    console.error(err.stack);
    res.status(500).send({error: err.message})
  });
}
app.use(require('body-parser').json());
app.use(API, authRoutes);


